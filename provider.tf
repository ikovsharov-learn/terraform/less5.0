provider "google" {
    project = "theta-dialect-314313"
    region  = "europe-central2"
    zone    = "europe-central2-a"
}